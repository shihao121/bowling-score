package com.thougthtworks;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BowlingTest {

    @Test
    void should_return_0_when_no_throw() {
        Bowling bowling = new Bowling();
        int score = bowling.getScore();
        assertEquals(0, score);
    }

    @Test
    void should_return_throw_score_when_throw_once() {
        Bowling bowling = new Bowling();
        bowling.throwBowling(new int[] {3});
        assertEquals(3, bowling.getScore());
    }

    @Test
    void should_return_total_score_when_throw_one_frames() {
        Bowling bowling = new Bowling();
        bowling.throwBowling(new int[] {2, 3});
        assertEquals(5, bowling.getScore());
    }

    @Test
    void should_return_total_number_when_throw_ten_frames_without_strike_spare() {
        Bowling bowling = new Bowling();
        bowling.throwBowling(new int[] {1, 2}, new int[] {3, 4}, new int[] {5, 3},
                new int[] {4, 4}, new int[] {4, 3}, new int[] {8, 1}, new int[] {9, 0},
                new int[] {4, 2}, new int[] {6, 1}, new int[] {2, 4});
        assertEquals(70, bowling.getScore());
    }

    @Test
    void should_return_total_number_when_throw_ten_frames_with_first_frame_is_spare() {
        Bowling bowling = new Bowling();
        bowling.throwBowling(new int[] {1, 9}, new int[] {3, 4});
        assertEquals(20, bowling.getScore());
    }

    @Test
    void should_return_total_score_when_throw_ten_frames_with_spare_without_strike_and_last_frame_not_spare() {
        Bowling bowling = new Bowling();
        bowling.throwBowling(new int[]{1, 9}, new int[]{2, 8}, new int[]{3, 4},
                new int[]{4, 2}, new int[]{7, 3}, new int[]{6, 2}, new int[]{3, 4},
                new int[]{3, 7}, new int[]{5, 5}, new int[]{2, 2});
        assertEquals(100, bowling.getScore());
    }

    @Test
    void should_return_total_score_when_throw_ten_frames_with_spare_without_strike() {
        Bowling bowling = new Bowling();
        throwTenFrameWithSpare(bowling, 2);
        assertEquals(108, bowling.getScore());
    }

    @Test
    void should_return_total_score_when_throw_ten_frames_with_spare_and_spare_bonus_is_ten_without_strike() {
        Bowling bowling = new Bowling();
        throwTenFrameWithSpare(bowling, 10);
        assertEquals(116, bowling.getScore());
    }

    @Test
    void should_return_total_score_when_first_frame_is_strike() {
        Bowling bowling = new Bowling();
        bowling.throwBowling(new int[] {10}, new int[] {2, 3}) ;
        assertEquals(20, bowling.getScore());
    }

    @Test
    void should_return_total_score_with_strike_and_last_not_strike_not_spare() {
        Bowling bowling = new Bowling();
        bowling.throwBowling(new int[]{10}, new int[]{2, 3}, new int[]{10},
                new int[]{3, 7}, new int[]{3, 6}, new int[]{10}, new int[] {2, 8},
                new int[]{2, 5}, new int[] {10}, new int[] {2, 4});
        assertEquals(123, bowling.getScore());
    }

    @Test
    void should_return_total_score_with_strike_and_last_not_strike_is_spare() {
        Bowling bowling = new Bowling();
        bowling.throwBowling(new int[]{10}, new int[]{2, 3}, new int[]{10},
                new int[]{3, 7}, new int[]{3, 6}, new int[]{10}, new int[] {2, 8},
                new int[]{2, 5}, new int[] {10}, new int[] {2, 8}, new int[] {2});
        assertEquals(133, bowling.getScore());
    }

    @Test
    void should_return_total_score_with_strike_and_last_is_strike() {
        Bowling bowling = new Bowling();
        bowling.throwBowling(new int[]{10}, new int[]{2, 3}, new int[]{10},
                new int[]{3, 7}, new int[]{3, 6}, new int[]{10}, new int[] {2, 8},
                new int[]{2, 5}, new int[] {10}, new int[] {10}, new int[] {2, 6});
        assertEquals(141, bowling.getScore());
    }

    @Test
    void should_return_total_score_with_strike_and_bonus_is_strike() {
        Bowling bowling = new Bowling();
        bowling.throwBowling(new int[]{10}, new int[]{2, 3}, new int[]{10},
                new int[]{3, 7}, new int[]{3, 6}, new int[]{10}, new int[] {2, 8},
                new int[]{2, 5}, new int[] {10}, new int[] {10}, new int[] {10, 10});
        assertEquals(161, bowling.getScore());
    }

    @Test
    void should_get_300_when_given_ten_strikes() {
        Bowling bowling = new Bowling();
        bowling.throwBowling(new int[] {10}, new int[] {10}, new int[] {10},
                new int[] {10}, new int[] {10}, new int[] {10}, new int[] {10},
                new int[] {10}, new int[] {10}, new int[] {10}, new int[] {10, 10});
        assertEquals(300, bowling.getScore());
    }

    private void throwTenFrameWithSpare(Bowling bowling, int bonus) {
        bowling.throwBowling(new int[]{1, 9}, new int[]{2, 8}, new int[]{3, 4},
                new int[]{4, 2}, new int[]{7, 3}, new int[]{6, 2}, new int[]{3, 4},
                new int[]{3, 7}, new int[]{5, 5}, new int[]{2, 8}, new int[]{bonus});
    }



}