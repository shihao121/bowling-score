package com.thougthtworks;

import java.util.Arrays;
import java.util.stream.IntStream;

public class Bowling {

    private final static int FRAME_ROUND = 10;
    private final static int FRAME_FULL_MARK = 10;
    private final static int TWO_CONTINUOUS_FULL_MARK = 10 * 2;

    private int score;

    public int getScore() {
        return this.score;
    }

    public void throwBowling(int[]... frameScores) {
        this.score += IntStream.range(0, Math.min(frameScores.length, Bowling.FRAME_ROUND))
                .map(index -> {
                    if (frameScores[index][0] == Bowling.FRAME_FULL_MARK) {
                        return frameScores[index + 1].length == 1
                                ? Bowling.TWO_CONTINUOUS_FULL_MARK + frameScores[index + 2][0]
                                : Bowling.FRAME_FULL_MARK + Arrays.stream(frameScores[index + 1]).sum();
                    }
                    int frameSum = Arrays.stream(frameScores[index]).sum();
                    return frameSum == Bowling.FRAME_FULL_MARK ? frameSum + frameScores[index + 1][0] : frameSum;
                }).sum();
    }
}
